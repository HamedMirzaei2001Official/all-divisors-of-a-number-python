import math
def divs(number):
    n = 1
    while(n<number):
        if(number%n==0):
            yield n
        else:
            pass
        n += 1
    yield number

if __name__ == "__main__":
    print(list(divs(6)))